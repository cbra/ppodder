# ppodder Podcatcher

Started around 2012 as a private hack to replace the CLI podcatcher [hpodder][]
that I've been using for several years, but keep the existing SQLite database
and basic functionality (add and update casts, download episodes), and support
UTF-8, (which didn't work in hpodder).

Over 10 years evolved into a messy bunch of workarounds and bugs that worked
just fine enough.

Refactored in 2022, including adding features and removing bugs, this will
likely continue to work fine until the last podcast is locked away in a closed
environment without RSS feed.

[hpodder]: https://jgoerzen.github.io/hpodder/

---

### Basic usage

To run the program Python (recommended 3.8+) and some dependencies are required,
see pyproject.toml/requirements.txt. Install requirements, provide a virtual
environment, or use one of the many dependency management tools ([Poetry][] is
recommended and used in the examples).

[Poetry]: https://python-poetry.org/

The first run does a small interactive setup, and creates a config file (at a
default location) and an empty database (at the specified location). Just press
Enter to confirm the proposed default locations for database and downloads, or
provide individual paths.

```
$ poetry run ./podcatcher
Podcatcher setup.
Download directory (default /home/me/podcasts)? 
Database directory (default /home/me/.local/share/ppodder)? 
Writing config to /home/me/.config/ppodder/ppodder.conf
Done.
```


The basic usage pattern is

- add a podcast by providing a feed URL
```
$ poetry run ./podcatcher a "https://some.podcast.net/rssfeed"
Added podcast 1 Some Podcast
```

- update podcast(s) to detect new episodes
```
$ poetry run ./podcatcher u
Updating 1 Some Podcast | https://some.podcast.net/rssfeed
Found 3 new episodes.
Added     0 Introducing Some Podcast (published 2022-01-02T12:34:56)
Added     1 SP1 An episode title (published 2022-01-10T12:34:56)
Added     2 SP2 The second episode (published 2022-01-14T12:34:56)
```

- download
```
$ poetry run ./podcatcher dl
Loading 1 Some Podcast/0 Introducing Some Podcast from https://cdn.podigee.com/media/podcast_12345_intro.mp3?v=1234567890&source=feed
Saved to /home/me/podcasts/Some Podcast/0_Introducing Some Podcast.mp3
Loading 1 Some Podcast/1 SP1 An episode title from https://cdn.podigee.com/media/podcast_12345_ep1.mp3?v=1234567890&source=feed
Saved to /home/me/podcasts/Some Podcast/1_SP1 An episode title.mp3
Loading 1 Some Podcast/2 SP2 The second episode from https://cdn.podigee.com/media/podcast_12345_ep2.mp3?v=1234567890&source=feed
Saved to /home/me/podcasts/Some Podcast/2_SP2 The second episode.mp3
```

### Advanced usage

Run `./podcatcher -h` or `./podcatcher <subcommand> -h` for detailed help.


```
$ poetry run ./podcatcher -h
usage: podcatcher [-h] [-i]                    ...

Podcatcher

optional arguments:
  -h, --help          show this help message and exit
  -i, --info          Print info

Commands:
                    
    add (a)           Add a new podcast.
    update (u)        Update podcasts. By default all active podcasts are updated, or pass a list of podcast IDs to select specific podcasts to update.
    download (dl)     Download pending episodes. By default download episodes of all active podcasts, or pass a list of podcast IDs to select specific podcasts to download.
    lscasts (ls)      List podcasts. By default list active podcasts, pass -a/--all to list all podcasts. Allows filter by tags or specific podcast IDs.
    checkfeed (cf)    Check feed. By default print info and list episodes with parsed pubDate timestamps and GUID (useful to check if the feed provides pubDate and GUIDs this program can work with), or pass -r/--raw to print raw feed data.
    lseps (le)        List episodes. By default list episodes of all active podcasts, or pass a list of podcast IDs to list episodes for specific podcasts.
    dleps (de)        Download episodes. Specify a podcast and some episodes by ID and try to download these episodes, independent of their state. Can be used to download previously failed episodes.
    tag               Tag podcasts or episodes, and organize tags. Tags are always lowercased. Tags are automatically added to the database when a podcast or episode is tagged. Also keywords of podcasts and episodes are automatically added as tags.
    remove (rm)       Remove podcasts or episodes. Caution when deleting episodes: this deletes entries from the database and there's no way of re-inserting old episodes. A podcast can be re-added of course.
    setepstate (ses)  Set state for specified episodes.


$ poetry run ./podcatcher a -h
usage: podcatcher add [-h] url [url ...]

positional arguments:
  url         Podcast feed URL

optional arguments:
  -h, --help  show this help message and exit


$ poetry run ./podcatcher u -h
usage: podcatcher update [-h] [-c CID [CID ...]] [-t TAG [TAG ...]]

optional arguments:
  -h, --help            show this help message and exit
  -c CID [CID ...], --cid CID [CID ...]
                        Podcast ID
  -t TAG [TAG ...], --tag TAG [TAG ...]
                        Filter by tags


$ poetry run ./podcatcher dl -h
usage: podcatcher download [-h] [-c CID [CID ...]] [-t TAG [TAG ...]]

optional arguments:
  -h, --help            show this help message and exit
  -c CID [CID ...], --cid CID [CID ...]
                        Podcast ID
  -t TAG [TAG ...], --tag TAG [TAG ...]
                        Filter by tags
```
