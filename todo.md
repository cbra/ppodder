## todo

- better usage messages
- more feedback in commands
- include pattern for original episode filename (if available)
- don't print multiple emplty lines when processing multiple empty casts (e.g. download)
- update podcast title if changed
- add dryrun option for download
- check which commands could work without extra options (e.g. some commands 
  shouldn't need and extra -c to process cast ID arguments)
- allow subdirs for podcast folders (clean without folder separator)?
- more details in README, e.g. config patterns, tipps for initial setup (xargs...)
- pass podcast instances around instead of cast IDs?
- add tests
- automatically update podcasts after adding?
- async podcast updates, episode downloads
- add handler to close in case of SIGTERM, SIGKILL, ...
- support podlove chaptermarks (z.b. spoileralert)
  - are there shownotes in podcasts?
- see e.g. freakshow feed https://feeds.metaebene.me/freakshow/oga for more extensions
- check out sqlite3 converters as an alternative to tuple unpacking:
  https://docs.python.org/3/library/sqlite3.html#converting-sqlite-values-to-custom-python-types
- requests with verify=False to skip SSL verification? (e.g. /dev/radio)
- support adding of missing episodes in update... somehow?
- ShowHN-Post?

## done

- data: search item for enclosure->url (or guid?!?).
- delete cast and its episodes
- add cmdline to set episode status
- add counter for failed episode download attempts?
- add logging
- make inactive casts visible
- use argparse, add subcommands
  - cf. https://docs.python.org/3/library/argparse.html#sub-commands
  - in case nested subcommands are necessary:
    https://stackoverflow.com/questions/10448200/how-to-parse-multiple-nested-sub-commands-using-python-argparse
- find a way to determine if an episode has already downloaded that doesn't
  rely on the url because they change all the time
  - use GUID!
  - i.e. podlove podcasts change url from time to time, see CRE, Forschergeist,
    Fokus Europa, ...
- store pubdate in episodes
- format per enclosure.type
- support keywords (e.g. itunes, ...)
- reset failed attempts counter after a successful update/attempt
- use podcast given name for folders
- what is the message column in podcasts for?
- allow ep id or pubdate as prefix for ep titles
- add tags to podcasts
- make feed type
- add overall timeout for episode download
- ? add summary/description columns to podcasts and episodes tables.
- use item pubDate and feed last update to check new items
  - note: sorting in a feed is often last-first!
- get_episode_titles and ids don't return sets, let caller make set
- use <itunes:keywords> (comma-separated, html-encoded) as tags
- interactive initialization
- put constants in config
- support update of podcast url (<itunes:new-feed-url>)
  - support changing of feed url if forwarded?
- make filename pattern configurable per cast
- itunes also has optional itunes:episode/itunes:season, both should be non-zero
  integers, store and support as potential filename part
- support download of specific episodes (must be in DB)
- download and update casts by tags
- black/pep8 check/autoformatting
- set episode state, remove episodes/podcasts from database
- keep DB connection open
- comment
- add option to list only podcasts with pending episodes