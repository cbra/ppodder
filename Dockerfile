FROM python:3.8.10

RUN adduser --home /app --disabled-password app
USER app
WORKDIR /app
ENV PATH="/app/.local/bin:$PATH"
RUN pip --no-cache-dir --disable-pip-version-check install \
        black==22.3.0 \
        flake8==4.0.1 \
        mypy==0.940 \
        poetry==1.1.13

RUN echo 'alias ll="ls -lisaF"' >> ~/.bashrc
