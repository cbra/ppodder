# dev tools docker

- to avoid installing flake8, black, and othe tools on all dev machines, put 
  them in a container, mount the project dir, and run the tools from within the 
  container
- run this once

    
    $ docker build -t "python3.8:devtools" .

- then run

    
    $ docker run --network host -ti -v $(pwd):/app python3.8:devtools /bin/bash
    $ cd ppodder
    $ make lint
    $ make fmt

# RSS feed tool

- find the RSS feed backing apple podcasts or soundcloud
- https://getrssfeed.com/


# DB-Backups:
sqlite3 -line hpodder.db 'select episodeid, castid, title, epurl, status, eplength from episodes;' > episodes-backup.txt
sqlite3 -line hpodder.db 'select * from podcasts;' > podcasts-backup.txt

# Von den Episodennamen retten, was zu retten ist...
python3 convert_db_backup.py episodes-backup.txt > episodes.txt

create table podcasts (cast_id integer, orig_name text, feed_url text, active integer, last_update integer, last_attempt integer, failed_attempts integer);
create table episodes (ep_id integer, cast_id integer, title text, ep_url text, status integer, size integer);

# fill tables from backups...

# add given name column
alter table podcasts add column given_name text;
update podcasts set given_name = orig_name;

# add a message column for error messages
alter table podcasts add column message text;

# create indexes
create index idx_cast_id on podcasts (cast_id);
create index idx_ep_id on episodes (ep_id);

# install

## venv
sudo apt install python3-pip
sudo apt-get install python3-venv
python3 -m venv env
pip install -r requirements.txt

## poetry
poetry init
poetry add requests@2.8.1 python-dateutil@2.4.2 appdirs@1.4.3
poetry run python podcatcher.py

## itunes
- xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
  is not an actual DTD, just a namespace, see
  https://stackoverflow.com/questions/8389872/where-is-the-official-podcast-dtd
- https://help.apple.com/itc/podcasts_connect/#/itcb54353390
- https://podcasters.apple.com/support/823-podcast-requirements

## sqlite3
- print all table schema: `$ sqlite3 podcasts.db .schema`
- to execute foreign key relations, they must be explicitly switched on:
  https://www.python-forum.de/viewtopic.php?t=50603
- https://stackoverflow.com/questions/6242756/how-to-retrieve-inserted-id-after-inserting-row-in-sqlite-using-python
- https://stackoverflow.com/questions/19337029/insert-if-not-exists-statement-in-sqlite
- Date/Time doesn't really exist: https://www.sqlite.org/datatype3.html#date_and_time_datatype
- "sqlite3.OperationalError: row value misused" - don't use parentheses in group by/order by, see
  https://stackoverflow.com/questions/42123011/row-value-misused-error-in-sqlite-database
- "sqlite3.OperationalError: foreign key mismatch" - must include all PK columns in a FK relation, see
  https://stackoverflow.com/questions/6848608/sqlite-foreign-key-mismatch-error

## Argparse
- improvement https://mike.depalatis.net/blog/simplifying-argparse.html
- alternatives https://realpython.com/comparing-python-command-line-parsing-libraries-argparse-docopt-click/
- autocomplete https://kislyuk.github.io/argcomplete/

## clean filenames
- https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
- https://stackoverflow.com/questions/7406102/create-sane-safe-filename-from-any-unsafe-string/71199182#71199182
- `clean = re.sub(r"[/\\?%*:|\"<>\x7F\x00-\x1F]", "-", dirty)`

## Named tuples
- https://stackoverflow.com/questions/2970608/what-are-named-tuples-in-python
- https://realpython.com/python-namedtuple/

## type hints
- https://stackoverflow.com/questions/32557920/what-are-type-hints-in-python-3-5

## feed extension namespaces
- 'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
- 'atom': 'http://www.w3.org/2005/Atom',
- 'content': 'http://purl.org/rss/1.0/modules/content/',
- 'googleplay': 'http://www.google.com/schemas/play-podcasts/1.0',
- 'media': 'http://search.yahoo.com/mrss/',
- 'feedburner': 'http://rssnamespace.org/feedburner/ext/1.0',
- podlove stuff, e.g. xmlns:psc="http://podlove.org/simple-chapters"

## valid substitutions for podcast dir/episode filename patterns in config
    _fnpatsubstitutions = [
        'pcid',  # podcast ID (by database)
        'pctitle',  # fn-safe podcast title
        'epid',  # episode ID (by database)
        'eptitle',  # fn-safe episode title
        'date',  # parsed pubDate (if provided and possible) as YYYY-MM-DD
        'seasonepisode',  # S<itunes:season>E<itunes:episode> if provided
        'ext',  # extension, determined by enclosure.type or original filename
    ]
