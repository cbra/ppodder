#!/usr/bin/make -rf

all:
	poetry install

lint:
	flake8 podcatcher
	black --check podcatcher

fmt:
	black podcatcher

clean:
	rm podcasts.db

.PHONY: all lint clean
